package com.szczepanczykd.snake;


import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;


public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_menu);

    }
    public void startGame(View view)
    {


        Intent i = new Intent(MainActivity.this,snakeActivity.class);
        Bundle b = new Bundle();
        b.putInt("Width", this.getWindow().getDecorView().getWidth());
        i.putExtras(b);
        startActivity(i);
        finish();
    }
    public void quitGame(View view)
    {
        finish();
    }

}
