package com.szczepanczykd.snake;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.renderscript.ScriptIntrinsicYuvToRGB;
import android.view.View;
import android.widget.TextView;

import org.w3c.dom.Text;


public class caseActivity extends Activity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.case_menu);

        TextView view = (TextView) findViewById(R.id.scoreView);

        Bundle b = getIntent().getExtras();
        int points = b.getInt("score");
        String score = getString(R.string.score);
        view.setText(score + Integer.toString(points));

    }
    public void quitGame(View view)
    {
        finish();
    }
    public void startGame(View view)
    {
        Intent i = new Intent(this,snakeActivity.class);
        Bundle b = new Bundle();
        b.putInt("Width", this.getWindow().getDecorView().getWidth());
        i.putExtras(b);
        startActivity(i);
        finish();
    }

}
