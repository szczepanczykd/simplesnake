package com.szczepanczykd.snake;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import java.util.ArrayList;
import java.util.Random;

/**
 * Created by szczepanczykd on 23.02.16.
 */

public class snakeEngine {

    private ArrayList<Integer> x_vec; //kontener na zmienne
    private ArrayList<Integer> y_vec;
    private Bitmap headBitmap;
    private Bitmap bodyBitmap;
    private Bitmap cakeBitmap;
    private boolean lockdown; //lockdown zabezpiecza przed zawróceniem węża na swoje ciało.
    private boolean status; //status określa, czy zjadł ciastko.
    private boolean lockFinal;
    private int randx;
    private int randy;
    private int poolx;
    private int pooly;
    private int dot_size;

    public int score;

    DIR dir;

    enum DIR {
        UP (0), LEFT (1), DOWN(2), RIGHT(3),NO(4);

        private final int dirCode;

        DIR(int dirCode)
        {
            this.dirCode = dirCode;
        }
        public int getDirCode()
        {
            return dirCode;
        }
    };


    ////////////////////////////////////////////////////////////////////////////////
    public snakeEngine(Resources resources, int dot_size)
    {
        score = 0;
        lockFinal = false;
        dir = DIR.DOWN;
        x_vec = new ArrayList<>();
        y_vec = new ArrayList<>();

        randx = 5;
        randy = 8;

        lockdown = true;
        this.dot_size = dot_size;
        setImages(resources); //ustawimy tutaj grafikę. Można podać dowolną. Samo się wyskaluje na podstawie dot_size.
        setUpSnake();
    }

    public void setDirection(int set_direction){

        if((dir.getDirCode() + 2 != set_direction) & dir.getDirCode() - 2 != set_direction & dir.getDirCode() != set_direction ) //sprawdza, czy kierunek nie jest zwrotny
        {
            if(!lockdown) { //dla pewności. Jak szybko się kliknie, to można zawrócić węża na siebie.
                // 0 gora, 1 lewo, 2 dół, 3 prawo.

                if(set_direction == 0)
                {
                    dir = DIR.UP;
                }
                else if(set_direction == 1)
                {
                    dir = DIR.LEFT;
                }
                else if(set_direction == 2)
                {
                    dir = DIR.DOWN;
                }
                else
                {
                    dir = DIR.RIGHT;
                }

                lockdown = true; //lockdown !
            }
        }

    }

    public void unlock() //chcę odblokować możliwość ruchu !
    {
        lockdown = false;
    }


    public boolean moveSnake(Canvas canvas, Paint drawPaint, int poolx, int pooly) // rysuję węża.
    {

        if(lockFinal)
        dir = DIR.NO;

        this.poolx = poolx;
        this.pooly = pooly;


        canvas.drawBitmap(cakeBitmap,randx * dot_size,randy * dot_size,drawPaint);

        if (status)
        {
            for(int i = x_vec.size() - 2; i>0;i--) {

                x_vec.set(i,x_vec.get(i - 1));
                y_vec.set(i, y_vec.get(i - 1));

            }
            status = false;
        }
        else {
            for (int i = x_vec.size() - 1; i > 0; i--) {

                x_vec.set(i, x_vec.get(i - 1));
                y_vec.set(i, y_vec.get(i - 1));

            }
        }


            if (dir == DIR.UP)
                y_vec.set(0, (Integer) y_vec.get(0) - dot_size);
            else if (dir == DIR.LEFT)
                x_vec.set(0, (Integer) x_vec.get(0) - dot_size);
            else if (dir == DIR.DOWN)
                y_vec.set(0, (Integer) y_vec.get(0) + dot_size);
            else if (dir == DIR.RIGHT)
                x_vec.set(0, (Integer) x_vec.get(0) + dot_size);



        for (int i = x_vec.size() - 1; i > 0; i--) {
            canvas.drawBitmap(bodyBitmap, (Integer) x_vec.get(i), (Integer) y_vec.get(i), drawPaint); //rysuje Cialo !
        }

        if(check_coords()) {
            lockFinal = true;
            return false;
        }
        else {
            canvas.drawBitmap(headBitmap, (Integer) x_vec.get(0), (Integer) y_vec.get(0), drawPaint);
            return true;
        }

    }

    public static Bitmap scaleDown(Bitmap realImage, float maxImageSize, boolean filter) {//skalowanie bitmapy
        float ratio = Math.min(
                (float) maxImageSize / realImage.getWidth(),
                (float) maxImageSize / realImage.getHeight());
        int width = Math.round((float) ratio * realImage.getWidth());
        int height = Math.round((float) ratio * realImage.getHeight());

        Bitmap newBitmap = Bitmap.createScaledBitmap(realImage, width,
                height, filter);
        return newBitmap;
    }

    private void setUpSnake() //ustawiam mu ... 7 kuleczek na start.
    {
        int x = dot_size;
        int y = dot_size;
        for(int i = 0;i<3;i++) {
            x_vec.add(x);
            y_vec.add(y);
            y -= dot_size;
        }
    }

    private void setImages(Resources res)
    {
        bodyBitmap = BitmapFactory.decodeResource(res, R.drawable.dot);
        bodyBitmap = scaleDown( bodyBitmap, dot_size, true);            //skaluje do określonego rozmiaru. W tym przypadku jest to zmienna dot_size.

        headBitmap = BitmapFactory.decodeResource(res, R.drawable.reddot);
        headBitmap = scaleDown(headBitmap, dot_size, true);

        cakeBitmap = BitmapFactory.decodeResource(res, R.drawable.cake);
        cakeBitmap = scaleDown(cakeBitmap,dot_size,true);
    }



    private boolean check_coords() {

        int x = (Integer)x_vec.get(0); //sprawdza, czy snake wjechał w ciasteczko.
        int y = (Integer)y_vec.get(0);

        if((randx * dot_size) == x & (randy * dot_size) == y) {

            x_vec.add(x_vec.get(x_vec.size()-1)); //jeśli tak, to dodaje kolejny element do tablicy.
            y_vec.add(y_vec.get(y_vec.size()-1)); //o parametrach poprzedniego. (dupa rośnie)
            while(!checkRandom()) // generuje kolejną liczbę i sprawdza, czy nie jest na koordynatach węża.
            {                       //ciasteczko nie pojawi się na wężu przecież...
                generate(); // generuje aż do skutku.
            }
            score++; //zwiększa liczbę zjedzonych ciastek.
            status = true;
            return false;
        }

        for(int i = 1;i<x_vec.size();i++) //tutaj jest totalna masakra.
        {                                                   //sprawdza, czy wąż nie wjechał na swoje ciało.


            int z = (Integer)x_vec.get(i);
            int b = (Integer)y_vec.get(i);

            if(x < dot_size) {
                x_vec.add(x_vec.get(x_vec.size() - 1));
                y_vec.add(y_vec.get(y_vec.size()-1));
                status = true;
                return true;
            }
            else if(y < dot_size) {
                x_vec.add(x_vec.get(x_vec.size() - 1));
                y_vec.add(y_vec.get(y_vec.size()-1));
                status = true;
                return true;
            }
            else if(x > (poolx - 2)* dot_size) {
                x_vec.add(x_vec.get(x_vec.size() - 1));
                y_vec.add(y_vec.get(y_vec.size()-1));
                status = true;
                return true;
            }
            else if(y > (pooly - 2)* dot_size) {
                x_vec.add(x_vec.get(x_vec.size() - 1));
                y_vec.add(y_vec.get(y_vec.size()-1));
                status = true;
                return true;
            }

            if(x == z)
            {
                if(y == b) {
                    x_vec.add(x_vec.get(x_vec.size()-1));
                    y_vec.add(y_vec.get(y_vec.size() - 1));
                    status = true;
                    return true;
                }
            }

        }
        return false;
    }

    private void generate() //zwykły generator.
    {
        Random generator = new Random();
        randx = generator.nextInt(poolx - 2) + 1;
        randy = generator.nextInt(pooly - 2) + 1;
    }

    private boolean checkRandom() //sprawdzenie wygenerowanej liczby.
    {
        for (int i = 0; i<x_vec.size();i++)
        {
            int x = (Integer)x_vec.get(i);
            int y = (Integer)y_vec.get(i);

            if (x == (randx * dot_size) & y == (randy *dot_size))
            {
                return false;
            }

        }
        return true;
    }

}
