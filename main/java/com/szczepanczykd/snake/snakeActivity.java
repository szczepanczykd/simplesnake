package com.szczepanczykd.snake;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import android.util.Log;
import android.view.MotionEvent;

import java.util.logging.Handler;
import java.util.logging.LogRecord;


public class snakeActivity extends Activity {


private snakeView view;

    private float screenWidth;
    private float screenHeight;
    private float diff;
    private int sc;
    private boolean state;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.snake_view);

        view = (snakeView) findViewById(R.id.custView);
            Bundle b = getIntent().getExtras();
            int Width = b.getInt("Width");
            state = false;
            view.startSnake(Width / 25);

                view.setCustListener(new snakeView.snakeMsg() {
                    @Override
                    public void snakeStopped(int score) {

                        sc = score;
                        state = true;
                    }


                });

    }

    public void caseActivity()
    {
        state = false;
        Intent i = new Intent(snakeActivity.this, caseActivity.class);
        Bundle b = new Bundle();
        b.putInt("score", sc);
        i.putExtras(b);
        startActivity(i);
        finish();
    }

    @Override
    public boolean onTouchEvent(MotionEvent e)
    {

        screenHeight = this.getWindow().getDecorView().getHeight();
        screenWidth = this.getWindow().getDecorView().getWidth();
        float x = e.getX();
        float y = e.getY() - screenWidth;
        diff = screenHeight- screenWidth;

        if( (y + screenWidth) < screenWidth)
        {
            if(state)
            caseActivity();
            return false;
        }



        if( x * y > diff/2 * screenWidth/2 ) //to jest zajebiste.
        {
            if(y > x * ( diff / screenWidth)) //analiza po coś się przydała !
               // changeDirection(2);
            down();
            else// (y < x * ( diff / screenWidth))
              //  changeDirection(3);
               right();
        }
        else
        {
            if(y > x * ( diff / screenWidth))
             //   changeDirection(1);
               left();
            else //if(y < x * ( diff / screenWidth))
             //   changeDirection(0);
            up();
        }



        return true;
    }

    public void up()
    {
        view.setDirection(0);
    }
    public void down()
    {
        view.setDirection(2);
    }
    public void left()
    {
        view.setDirection(1);
    }
    public void right()
    {
        view.setDirection(3);
    }

}
