package com.szczepanczykd.snake;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import java.util.TimerTask;
import java.util.Timer;

/**
 * Created by szczepanczykd on 17.02.16.
 */
public class snakeView extends View {

    public interface snakeMsg {

    public void snakeStopped(int score);

    }



    private snakeMsg setter;
    private Bitmap directions;
    private Paint drawPaint;
    private int dot_size;
    private Timer timer;
    private TimerTask timerTask;
    private int poolx;
    private int pooly;


    private snakeEngine snakeEng;


    public snakeView(Context context, AttributeSet attrs){
        super(context, attrs);
        this.setter = null;
        this.setSystemUiVisibility( View.SYSTEM_UI_FLAG_FULLSCREEN );
        setUpPainter();
    }

    public void startSnake(int size)
    {

        dot_size = size;

        Resources res = getResources();
        snakeEng = new snakeEngine(res,dot_size);
        directions = BitmapFactory.decodeResource(res, R.drawable.touchzone);


        startTimer();
    }



    public void setCustListener(snakeMsg listener)
    {
        this.setter = listener;
    }

    private void setUpPainter() {
        drawPaint = new Paint();
        drawPaint.setColor(Color.BLACK);
        drawPaint.setAntiAlias(true);


    }

/////////////////////////////////////////////////
    @Override
    public void onDraw(Canvas canvas){



        canvas.drawLine(dot_size,0,dot_size,this.getWidth(),drawPaint);
        canvas.drawLine(0,dot_size,this.getWidth(),dot_size,drawPaint);
        poolx = this.getWidth()/dot_size;
        //pooly =  this.getHeight()/dot_size;
        pooly = poolx;
        canvas.drawLine(0,(pooly-1) * dot_size,this.getWidth(),(pooly-1) * dot_size,drawPaint);
        canvas.drawLine((poolx-1) * dot_size,0,(poolx-1) * dot_size, this.getWidth(),drawPaint);

        directions = Bitmap.createScaledBitmap(directions,this.getWidth(),this.getHeight()-this.getWidth(),true);
        canvas.drawBitmap(directions, 0, this.getWidth(), drawPaint);


       if(!snakeEng.moveSnake(canvas,drawPaint,poolx,pooly))
        {
           stopTimer();
            if(setter != null)
            setter.snakeStopped(snakeEng.score);
        }

        snakeEng.unlock();
    }

    public void startTimer(){
        timer = new Timer();

        timerTask = new TimerTask() {
            @Override
            public void run() {

                postInvalidate();
            }
        };
        timer.schedule(timerTask, 1000, 200);
    }
/////////////////////////////////////////////


/////////////////////////////////////////////////////
    public void stopTimer(){
        timerTask.cancel();
        timer.purge();

    }

    public void setDirection(int direction)
    {
        snakeEng.setDirection(direction);
    }

}
